#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAPFILENAME "map2.txt"
#define ROUTESFILENAME "routes.txt"
#define LF 10 // line feed - carriage to bottom
#define CR 13 // carriage return - carriage to left
#define ROVER '0'
#define TARGET 'X'
#define FREEZONE '1'
#define NOGO 'N'
#define MIN_MAP_SIZE_X 4
#define MIN_MAP_SIZE_Y 4
#define MAX_MAP_SIZE_X 20
#define MAX_MAP_SIZE_Y 20

typedef enum { Up, Right, Down, Left } Direction;
typedef struct  {
   int X : 6;//limit 6 bit
   int Y : 6;//same
   int STEP : 10;//step==count for list_of_steps
} Coords;//name of structure type

const char* directionNames[] = {"Up", "Right", "Down", "Left"};

int CountX = 0;// all x symbols
int CountY = 0;// all y symbols

Coords movement_coords[4] = {
    {0,-1},//Up
    {1,0},//Right
    {0,1},//Down
    {-1,0}//Left
};

Coords RoverCoords;
Coords TargetCoords;

Coords *list_of_steps;

int lastStepToStartPointFromFinish = 0;
int lastAddedElementNumber = 0;
int is_finish = 0;

char *outputString;

FILE *mapfile;//pointer file for map
FILE *routesfile;//pointer file for routes

// declare functions
void setCountOfSymbolsInFile();
Coords getCoordsBySymbol(char symbol);
int checkIfMapSizeIsCorrect();
char getSymbolByCoords(int x, int y);
int findPath();
int getPathBySteps(int step);
int iterateCellsByStep(int step);
int exploreAdjacentCellByDirection(Direction direction, int num);
int addToListOfSteps(int x, int y, int step);
int isInList(int x, int y, int step);
int checkIfFreeZone(int x, int y, int step);
void definePathSteps(int x, int y, int step);
void iterateAdjacentCells(int num);
void outputResult();

int main()
{
    //Open file for read only and check if NULL -> 
    if ((mapfile = fopen (MAPFILENAME,"r")) == NULL)
    {   
        printf ("Can not open %s file\n", MAPFILENAME);
	exit(1);
    }

    setCountOfSymbolsInFile();
    
    if( !checkIfMapSizeIsCorrect(CountX, CountY) ) 
    {
        printf("Not valid map sizes\n");
	exit(1);
    }
        
    Coords coords = getCoordsBySymbol(ROVER);
             
    if(!coords.X || !coords.Y) 
    {
        printf("No valid start point on map\n");
	exit(1);
    }
    
    RoverCoords.X = coords.X;
    RoverCoords.Y = coords.Y;
    
    coords = getCoordsBySymbol(TARGET);
    
    if(!coords.X || !coords.Y) 
    {
        printf("No valid end point on map\n");
	exit(1);
    }
    
    TargetCoords.X = coords.X;
    TargetCoords.Y = coords.Y;

    printf("RoverCoords X(%d), Y(%d)\n", (int)RoverCoords.X, (int)RoverCoords.Y);
    printf("TargetCoords X(%d), Y(%d)\n", (int)TargetCoords.X, (int)TargetCoords.Y);
    
    int path = findPath();
        
    if(!path) 
    {
        printf("No path on map\n");
	exit(1);
    }
    
    //show matrix of all steps
    int i;
    for( i = 0; i <= lastAddedElementNumber; i++) 
    {
        printf("(%d; %d; %d) \n", list_of_steps[i].X, list_of_steps[i].Y, list_of_steps[i].STEP);
    }
        
    definePathSteps(RoverCoords.X, RoverCoords.Y, lastStepToStartPointFromFinish);
    
    outputResult();
    
    printf("%s\n", outputString);
   
    // Files closing
    if ( fclose (mapfile) ) 
        printf ("%s file close error\n", MAPFILENAME);
    
    //free memory
    free(list_of_steps);
    free(outputString);

    return 0;

}

void setCountOfSymbolsInFile()
{
    CountY = 1;
    
    int countedX = 0; //flags

    int symcount = 0; //count of all symbols without last two in every string 
    int symcode;
	
    do
    {
        symcode = fgetc (mapfile);

        if (symcode == LF) continue;
        
        if (symcode == CR)
	{
            CountY++;
            countedX = 1;
	}
	else
	{
            if (!countedX) CountX++;
            symcount++;
        }
     
    } while (symcode != EOF); // while it's not the end of file
    
}

int checkIfMapSizeIsCorrect(int CountX, int CountY)
{
    printf("CountX %d CountY %d\n", CountX, CountY);
    
    if(CountX < MIN_MAP_SIZE_X || CountX > MAX_MAP_SIZE_X ||
       CountY < MIN_MAP_SIZE_Y || CountY > MAX_MAP_SIZE_Y)
    {
        return 0;
    }
    return 1;
}

char getSymbolByCoords(int x, int y)
{
    long offset = (x - 1) + ((y - 1) * (CountX + 2));

    if(fseek(mapfile, offset, SEEK_SET) != 0) printf ("Can't fseek in getSymbolByCoords.\n");

    int symcode = fgetc(mapfile);

    return (char)symcode;
}

Coords getCoordsBySymbol(char symbol) 
{
    int symcode, x, y;
    
    Coords coords;

    coords.X = 0;
    coords.Y = 0;

    for (y = 1; y <= CountY; y++)
    {
        for (x = 1; x <= CountX; x++)
        {
            symcode = getSymbolByCoords(x,y);
            
            if((char)symcode == symbol)
            {
                coords.X = x;
                coords.Y = y;
                break;
            }
        }
    }
	
    return coords;
}

int findPath()
{
    int size = 1;
    list_of_steps = (Coords *)malloc(size*sizeof(Coords));

    //from end to start!!!
    list_of_steps[0] = TargetCoords;
    return getPathBySteps(0);
}

int getPathBySteps(int step)
{     
    printf("%d step is iterating \n", step);
    
    int step_exist = iterateCellsByStep(step);
    
    if(is_finish)
    {
        printf("FINISH\n");
        return 1;
    }
    
    if(!step_exist) 
    {
        printf("No path from ROVER to TARGET\n");
        return 0;
    }

    ++step;
    
    getPathBySteps(step);
}

int iterateCellsByStep(int step)
{    
    int i, step_exist = 0;

    for( i = 0; i <= lastAddedElementNumber; i++) 
    {
        if(list_of_steps[i].STEP == step &&
           list_of_steps[i].X > 0 && list_of_steps[i].Y > 0)
        {
            iterateAdjacentCells(i);
            step_exist = 1;
        }
    }
    
    return step_exist;
}

void iterateAdjacentCells(int num)
{
    int i;
    
    for(i = Up; i <= Left; i++)
    {
        if(exploreAdjacentCellByDirection(i, num) == 2)
            is_finish = 1;
    }
}

int exploreAdjacentCellByDirection(Direction direction, int num)
{    
    int x = movement_coords[direction].X + list_of_steps[num].X;
    int y = movement_coords[direction].Y + list_of_steps[num].Y;
    
    int count = list_of_steps[num].STEP;

    int result = checkIfFreeZone(x, y, count+1);

    if(result > 0) 
    {
        addToListOfSteps(x, y, count+1);
        if(result == 2)
            return result;//finish all iteratings
    }
    return 0;
}

int checkIfFreeZone(int x, int y, int step)
{
    printf("(%d; %d) checkIfFreeZone\n",x,y);
    
    if(x <= 0 || y <= 0 || x > CountX || y > CountY )
    {
        printf("out of box\n");
        return 0;
    }
    
    char symbol = getSymbolByCoords(x,y);
    
    if(symbol == NOGO) 
    {
        printf("NOGO\n");
        return 0;
    }
    else if(symbol == FREEZONE)
    {
        printf("FREEZONE here\n");
        return 1;
    }
    else if(symbol == ROVER)
    {
        lastStepToStartPointFromFinish = step;
        printf("ROVER here, got it on %d step\n", lastStepToStartPointFromFinish);
        return 2;
    }
    
    return 0;
}

int addToListOfSteps(int x, int y, int step) 
{
    if(isInList(x, y, step)) return 0;

    printf("%d lastAddedElementNumber\n", lastAddedElementNumber);

    ++lastAddedElementNumber;
    
    list_of_steps = realloc(list_of_steps, (lastAddedElementNumber+1) * sizeof(Coords));
    
    list_of_steps[lastAddedElementNumber].X = x;
    list_of_steps[lastAddedElementNumber].Y = y;
    list_of_steps[lastAddedElementNumber].STEP = step;
    
    printf("(%d; %d; %d) added to list_of_steps\n", x, y, step);
    
    return 1;
}

int isInList(int x, int y, int step) 
{
    int i;

    for(i = 0; i <= lastAddedElementNumber; i++) 
    {
        if(x == list_of_steps[i].X && y == list_of_steps[i].Y)
        {          
            printf("(%d; %d) in list_of_steps\n", x, y);
            if(list_of_steps[i].STEP > step)
            {
                list_of_steps[i].STEP = step;
            }
            return 1; 
        }
    }

    return 0;
}


void definePathSteps(int x, int y, int step)
{    
    
    int size = strlen("The rover needs to follow these steps: ");
    outputString = (char *)malloc(size*sizeof(char));
    
    strcpy(outputString, "The rover needs to follow these steps: ");
    
    int i,j;
    
    --step;

    for( i = lastAddedElementNumber; i >= 0 ; i--) 
    {
        if(list_of_steps[i].STEP == step)
        {
            for(j = Up; j <= Left; j++)
            {                    
                if(x + movement_coords[j].X == list_of_steps[i].X && 
                   y + movement_coords[j].Y == list_of_steps[i].Y)
                {

                    x = list_of_steps[i].X;
                    y = list_of_steps[i].Y;
                    --step;
                    
                    size += (strlen(directionNames[j]) + 2) * sizeof(char);
                    outputString = realloc(outputString, size);
                    
                    strcat(outputString, directionNames[j]);
                    strcat(outputString, " ");
                }
            }
        }
    }
    
}

void outputResult()
{
    if ((routesfile = fopen (ROUTESFILENAME,"a")) == NULL)
    {   
        printf ("Can not open %s file\n", ROUTESFILENAME);
	exit(1);
    }
    
    fprintf(routesfile, "%s\n", outputString);
    
    if ( fclose (routesfile) ) 
        printf ("%s file close error\n", ROUTESFILENAME);
}

