/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   newsimpletest1.c
 * Author: FORA
 *
 * Created on 24 января 2016 г., 2:04
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAPFILENAME "map2.txt"
#define ROUTESFILENAME "routes.txt"
#define LF 10 // line feed - carriage to bottom
#define CR 13 // carriage return - carriage to left
#define ROVER '0'
#define TARGET 'X'
#define FREEZONE '1'
#define NOGO 'N'
#define MIN_MAP_SIZE_X 4
#define MIN_MAP_SIZE_Y 4
#define MAX_MAP_SIZE_X 20
#define MAX_MAP_SIZE_Y 20

typedef enum { Up, Right, Down, Left } Direction;
typedef struct  {
   int X : 6;//limit 6 bit
   int Y : 6;//same
   int STEP : 10;//step==count for list_of_steps
} Coords;//name of structure type

Coords *list_of_steps;
char *outputString;

int checkIfMapSizeIsCorrect();

void testCheckIfMapSizeIsCorrect() {
    int result = checkIfMapSizeIsCorrect(4,4);
    if (result != 1) {
        printf("%%TEST_FAILED%% time=0 testname=testCheckIfMapSizeIsCorrect (newsimpletest1) message=invalid map sizes\n");
    }
}

int findPath();

void testFindPathByRecursion() {
    int result = findPath();
    if (result != 0) {
        printf("%%TEST_FAILED%% time=0 testname=testFindPathByRecursion (newsimpletest1) message=error message sample\n");
    }
}

int getPathBySteps(int step);

void testGetPathBySteps() {
    int step = 0;
    int result = getPathBySteps(step);
    if (result != 0) {
        printf("%%TEST_FAILED%% time=0 testname=testGetPathBySteps (newsimpletest1) message=error message sample\n");
    }
}

int iterateCellsByStep(int step);

void testIterateCellsByStep() {
    int step = 0;
    int result = iterateCellsByStep(step);
    if (result != 0) {
        printf("%%TEST_FAILED%% time=0 testname=testIterateCellsByStep (newsimpletest1) message=error message sample\n");
    }
}



int addToListOfSteps(int x, int y, int step);

void testAddToListOfSteps() {
    int x = 1;
    int y = 1;
    int step = 0;
    int result = addToListOfSteps(x, y, step);
    if (result != 1) {
        printf("%%TEST_FAILED%% time=0 testname=testAddToListOfSteps (newsimpletest1) message=error message sample\n");
    }
}

int isInList(int x, int y, int step);

void testIsInList() {
    int x = 1;
    int y = 1;
    int step = 0;
    int result = isInList(x, y, step);
    if (result != 1) {
        printf("%%TEST_FAILED%% time=0 testname=testIsInList (newsimpletest1) message=error message sample\n");
    }
}

void definePathSteps(int x, int y, int step);

void testDefinePathSteps() {
    int x = 1;
    int y = 1;
    int step = 0;
    definePathSteps(x, y, step);
    if (!printf("%s\n", outputString)) {
        printf("%%TEST_FAILED%% time=0 testname=testDefinePathSteps (newsimpletest1) message=error message sample\n");
    }
}

int main(int argc, char** argv) {
    printf("%%SUITE_STARTING%% newsimpletest1\n");
    printf("%%SUITE_STARTED%%\n");

    printf("%%TEST_STARTED%%  testCheckIfMapSizeIsCorrect (newsimpletest1)\n");
    testCheckIfMapSizeIsCorrect();
    printf("%%TEST_FINISHED%% time=0 testCheckIfMapSizeIsCorrect (newsimpletest1)\n");

    printf("%%TEST_STARTED%%  testFindPathByRecursion (newsimpletest1)\n");
    testFindPathByRecursion();
    printf("%%TEST_FINISHED%% time=0 testFindPathByRecursion (newsimpletest1)\n");

    printf("%%TEST_STARTED%%  testGetPathBySteps (newsimpletest1)\n");
    testGetPathBySteps();
    printf("%%TEST_FINISHED%% time=0 testGetPathBySteps (newsimpletest1)\n");

    printf("%%TEST_STARTED%%  testIterateCellsByStep (newsimpletest1)\n");
    testIterateCellsByStep();
    printf("%%TEST_FINISHED%% time=0 testIterateCellsByStep (newsimpletest1)\n");

    printf("%%TEST_STARTED%%  testAddToListOfSteps (newsimpletest1)\n");
    testAddToListOfSteps();
    printf("%%TEST_FINISHED%% time=0 testAddToListOfSteps (newsimpletest1)\n");

    printf("%%TEST_STARTED%%  testIsInList (newsimpletest1)\n");
    testIsInList();
    printf("%%TEST_FINISHED%% time=0 testIsInList (newsimpletest1)\n");

    printf("%%TEST_STARTED%%  testDefinePathSteps (newsimpletest1)\n");
    testDefinePathSteps();
    printf("%%TEST_FINISHED%% time=0 testDefinePathSteps (newsimpletest1)\n");

    printf("%%SUITE_FINISHED%% time=0\n");

    return (EXIT_SUCCESS);
}
